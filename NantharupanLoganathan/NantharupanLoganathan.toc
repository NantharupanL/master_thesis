\select@language {english}
\contentsline {section}{Abstract}{ii}{Doc-Start}
\contentsline {section}{Acknowledgement}{iii}{Doc-Start}
\contentsline {chapter}{\numberline {1}Introduction}{2}{chapter.1}
\contentsline {section}{\numberline {1.1}Motivation for Research}{5}{section.1.1}
\contentsline {section}{\numberline {1.2}Research Objectives}{5}{section.1.2}
\contentsline {section}{\numberline {1.3}Research Question}{6}{section.1.3}
\contentsline {section}{\numberline {1.4}Research Design}{6}{section.1.4}
\contentsline {subsection}{\numberline {1.4.1}Research Stages}{6}{subsection.1.4.1}
\contentsline {subsection}{\numberline {1.4.2}Scope of the Research}{7}{subsection.1.4.2}
\contentsline {subsection}{\numberline {1.4.3}Data Collection and Analysis}{7}{subsection.1.4.3}
\contentsline {chapter}{\numberline {2}Literature Review}{8}{chapter.2}
\contentsline {section}{\numberline {2.1}Related Work in Autonomous computing}{8}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Re-Configuration}{9}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Methods used to make decision in Autonomous computing}{9}{subsection.2.1.2}
\contentsline {section}{\numberline {2.2}Utility Function}{9}{section.2.2}
\contentsline {paragraph}{Project MADAM }{10}{section.2.2}
\contentsline {paragraph}{Project MUSIC}{10}{section.2.2}
\contentsline {section}{\numberline {2.3}Decision Making}{12}{section.2.3}
\contentsline {section}{\numberline {2.4}Goals and Preference}{13}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Multi Criteria Decision Making Methods}{13}{subsection.2.4.1}
\contentsline {paragraph}{PROMETHEE}{15}{subsection.2.4.1}
\contentsline {paragraph}{Technique for order preference by similarity to ideal solution (TOPSIS)}{16}{subsection.2.4.1}
\contentsline {paragraph}{ELECTRE}{16}{subsection.2.4.1}
\contentsline {paragraph}{AHP}{16}{subsection.2.4.1}
\contentsline {chapter}{\numberline {3}Fuzzy Design and Concept development}{17}{chapter.3}
\contentsline {section}{\numberline {3.1}Sets}{17}{section.3.1}
\contentsline {section}{\numberline {3.2}Fuzzy Sets}{18}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Classes of Membership Functions}{18}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Linguistic Variables}{22}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Examples of Fuzzy sets}{22}{subsection.3.2.3}
\contentsline {paragraph}{Example One}{22}{subsection.3.2.3}
\contentsline {paragraph}{Example Two}{23}{figure.3.5}
\contentsline {section}{\numberline {3.3}PROMETHEE Method}{26}{section.3.3}
\contentsline {paragraph}{Step I:}{26}{section.3.3}
\contentsline {paragraph}{Step II:}{26}{section.3.3}
\contentsline {paragraph}{Type I}{27}{equation.3.3.14}
\contentsline {paragraph}{Type II}{27}{equation.3.3.15}
\contentsline {paragraph}{Type III}{28}{equation.3.3.16}
\contentsline {paragraph}{Type IV}{28}{equation.3.3.17}
\contentsline {paragraph}{Type V}{28}{equation.3.3.18}
\contentsline {paragraph}{Type VI}{28}{equation.3.3.19}
\contentsline {paragraph}{Step III:}{29}{equation.3.3.20}
\contentsline {paragraph}{Step IV :}{29}{equation.3.3.22}
\contentsline {section}{\numberline {3.4}Fuzzy Preference}{30}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Expressing preference using linguistic terms}{30}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Fuzzy system for preference function modelling}{30}{subsection.3.4.2}
\contentsline {subsection}{\numberline {3.4.3}Fuzzifier}{31}{subsection.3.4.3}
\contentsline {subsection}{\numberline {3.4.4}Fuzzy Preference Rule Base(FRPRB)}{31}{subsection.3.4.4}
\contentsline {subsection}{\numberline {3.4.5}Fuzzy Inference Engine(FIE)}{32}{subsection.3.4.5}
\contentsline {subsection}{\numberline {3.4.6}Defuzzifier}{34}{subsection.3.4.6}
\contentsline {section}{\numberline {3.5}Steps Involved in the PROMETHE method}{34}{section.3.5}
\contentsline {section}{\numberline {3.6}Fuzzy Preference Relation based Ranking}{37}{section.3.6}
\contentsline {subsection}{\numberline {3.6.1}Binary Fuzzy Relation}{37}{subsection.3.6.1}
\contentsline {subsection}{\numberline {3.6.2}Preference Modelling with Binary Fuzzy Relation}{38}{subsection.3.6.2}
\contentsline {chapter}{\numberline {4}Implementation }{40}{chapter.4}
\contentsline {section}{\numberline {4.1}SCALARM use case }{40}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Architecture}{40}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}SCALARM Deployment Cases}{41}{subsection.4.1.2}
\contentsline {paragraph}{Small Computation}{41}{figure.4.1}
\contentsline {paragraph}{Large Computation}{42}{figure.4.2}
\contentsline {paragraph}{Constraints}{42}{figure.4.3}
\contentsline {paragraph}{Variables are the values assigned to parameters before the deployment}{43}{Item.37}
\contentsline {paragraph}{Metrics are the values of the parameters that should be monitored and measured throughout the application runtime.}{43}{Item.37}
\contentsline {subsection}{\numberline {4.1.3}SCALARM Mathematical Utility Function}{43}{subsection.4.1.3}
\contentsline {section}{\numberline {4.2}Fuzzy Membership functional Models}{47}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Fuzzy PROMETHEE model implementation of SCALARM}{48}{subsection.4.2.1}
\contentsline {paragraph}{Make Span}{48}{subsection.4.2.1}
\contentsline {paragraph}{Cost}{48}{figure.4.5}
\contentsline {paragraph}{Performance Degradation}{49}{figure.4.6}
\contentsline {subsection}{\numberline {4.2.2}Fuzzy Criteria Models Implementation of SCALARM}{49}{subsection.4.2.2}
\contentsline {paragraph}{Make Span}{49}{subsection.4.2.2}
\contentsline {paragraph}{Deployment Cost}{50}{figure.4.8}
\contentsline {paragraph}{Performance Degradation}{51}{figure.4.9}
\contentsline {section}{\numberline {4.3}Prototype Implementation}{51}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Fuzzy Membership Functional Model}{51}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Fuzzy Criteria}{52}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Configuration}{53}{subsection.4.3.3}
\contentsline {subsection}{\numberline {4.3.4}Supportive Methods}{54}{subsection.4.3.4}
\contentsline {section}{\numberline {4.4}Fuzzy PROMETHEE Modelling and Implementation}{56}{section.4.4}
\contentsline {section}{\numberline {4.5}Example}{56}{section.4.5}
\contentsline {chapter}{\numberline {5}Validation}{62}{chapter.5}
\contentsline {section}{\numberline {5.1}Benchmark method}{62}{section.5.1}
\contentsline {section}{\numberline {5.2}Wilcoxon Signed Rank Test}{63}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Null and Alternative Hypotheses}{63}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}State alpha}{64}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Calculate the Test Statistic}{64}{subsection.5.2.3}
\contentsline {subsection}{\numberline {5.2.4}State Decision Rule}{65}{subsection.5.2.4}
\contentsline {section}{\numberline {5.3}Comparison}{65}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Mathematical utility versus Fuzzy Binary Preference Relation method}{66}{subsection.5.3.1}
\contentsline {subsubsection}{Sample size of 50}{66}{subsection.5.3.1}
\contentsline {subsubsection}{Sample Size of 250}{66}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Mathematical utility versus Fuzzy PROMETHEE}{67}{subsection.5.3.2}
\contentsline {subsubsection}{Sample size of 50}{67}{subsection.5.3.2}
\contentsline {subsubsection}{Sample Size of 250}{68}{subsection.5.3.2}
\contentsline {chapter}{\numberline {6}Conclusion and Future Research}{69}{chapter.6}
\contentsline {chapter}{Bibliography}{72}{Item.57}
\contentsline {chapter}{\numberline {A}Acronyms}{76}{appendix.A}
